import copy
import random


class Board:

    def __init__(self, boardstring):
        self.board = [[' ', ' ', ' '],
                      [' ', ' ', ' '],
                      [' ', ' ', ' ']]
        for i in range(3):
            for j in range(3):
                self.board[i][j] = boardstring[i*3+j]

    def print_board(self):
        print('---------')
        for line in self.board:
            print('| ', end='')
            print(' '.join(line), end='')
            print(' |')
        print('---------')

    def check_diagonals(self, player):
        if self.board[0][0] == self.board[1][1] == self.board[2][2] == player:
            return True
        elif self.board[0][2] == self.board[1][1] == self.board[2][0] == player:
            return True
        return False

    def check_horizontal(self, player):
        for i in range(3):
            if self.board[i][0] == self.board[i][1] == self.board[i][2] == player:
                return True
        return False

    def check_vertical(self, player):
        for i in range(3):
            if self.board[0][i] == self.board[1][i] == self.board[2][i] == player:
                return True
        return False

    def check_player(self, player):
        if self.check_diagonals(player) or self.check_horizontal(player) or self.check_vertical(player):
            return True
        return False

    def flat(self):
        return [self.board[i][j] for i in range(3) for j in range(3)]

    def check_win(self):
        flat_board = self.flat()
        count_o = len([el for el in flat_board if el == "O"])
        count_x = len([el for el in flat_board if el == "X"])

        # if not (-1 <= (count_o - count_x) <= 1):
        #     return "Impossible"
        if self.check_player("X") and self.check_player("O"):
            return "Impossible"
        if self.check_player("X"):
            return "X wins"
        if self.check_player("O"):
            return "O wins"

        if len([el for el in flat_board if el == " "]) == 0:
            return "Draw"
        return "Game not finished"
        # return False


class Game:
    board = Board("         ")
    player = "X"
    player1 = ''
    player2 = ''

    def __init__(self, boardstring="         "):
        Game.board = Board(boardstring)

    @staticmethod
    def set_player_difficulty(input_command):
        try:
            command, player1, player2 = input_command.split()
            if command == "start":
                Game.player1 = player1
                Game.player2 = player2
            return True
        except ValueError:
            print("Bad Parameters!")
            return False

    def human_move(self):
        while True:
            try:
                x, y = [int(a) for a in input("Enter the coordinates: ").split()]
            except ValueError:
                print("You should enter numbers!")
                continue
            if not isinstance(x, int) or not isinstance(y, int):
                print("You should enter numbers!")
                continue
            if x > 3 or y > 3 or x < 1 or y < 1:
                print("Coordinates should be from 1 to 3!")
                continue
            if Game.board.board[3 - y][x - 1] != " ":
                print("This cell is occupied! Choose another one!")
                continue
            Game.board.board[3 - y][x - 1] = self.player
            break

    def hard_move(self, board: Board, player):
        board_copy = Board("".join(board.flat()))
        other_player = "X" if player == "O" else "O"
        result = self.hard_move(board_copy, other_player)
        return result



    def ai_move(self, current):
        print(f'Making move level "{current}"')
        if current == "hard":
            pass

        elif current == "medium":
            other_player = "X" if self.player == "O" else "O"
            for i in range(3):
                for j in range(3):
                    medium_board = Board("".join(Game.board.flat()))
                    if medium_board.board[i][j] == " ":
                        medium_board.board[i][j] = self.player
                        if medium_board.check_win() == self.player + " wins":
                            Game.board.board[i][j] = self.player
                            del medium_board
                            return
                    del medium_board
            for i in range(3):
                for j in range(3):
                    medium_board = Board("".join(Game.board.flat()))
                    if medium_board.board[i][j] == " ":
                        medium_board.board[i][j] = other_player
                        if medium_board.check_win() == other_player + " wins":
                            Game.board.board[i][j] = self.player
                            del medium_board
                            return
                    del medium_board

        while True:
            x = random.randint(0, 2)
            y = random.randint(0, 2)
            if Game.board.board[y][x] != " ":
                continue
            Game.board.board[y][x] = self.player
            break

    def move(self):
        current_player = self.player1 if Game.player == "X" else self.player2
        self.human_move() if current_player == "user" else self.ai_move(current_player)


game = Game()
while not game.set_player_difficulty(input("Input command: ")):
    pass
while True:
    game.board.print_board()
    status = game.board.check_win()
    if status == "Game not finished":
        game.move()
        if game.player == "X":
            game.player = "O"
        elif game.player == "O":
            game.player = "X"
    else:
        print(status)
        break
