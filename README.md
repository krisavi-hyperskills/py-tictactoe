# Python Tic-Tac-Toe with AI

A famous game with a 3x3 field.
You can build a simple AI and play against it.
You’ll learn to create bots of various types of difficulty.

https://hyperskill.org/projects/73/